﻿using AutoOnePilotProj.Utilities;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutoOnePilotProj
{
    public class BasicFlow
    {
        private IWebDriver driver;

        /// <summary>
        /// Checks whether BMW cars have details on the Auto1 website.
        /// </summary>
        [Test]
        public void CheckingThatBmwCarsHaveDetails()
        {
            // Opens the browser and maximizes the window.
            driver = Common.OpenBrowser(driver);

            // Navigates to the Auto1 website.
            driver.Navigate().GoToUrl(url: "https://www.auto1.com/en/our-cars");

            // Selects the BMW manufacturer.
            Common.ClickOnUiElement(driver, Common.IdentifierType.xpath, PathDictionary.bmwSpanXpath);

            // Retrieves the title of the first sorting option selected.
            // This can be expanded into something more general like: "foreach [li] in <ul class="select2-selection__rendered">"
            // check that the only option selected is BMW OR among all the options there is one BMW.
            // Since we know that we only need to check for the BMW option we can avoid over-engineering.
            var firstSortingOption = Common.FindElementBy(Common.IdentifierType.xpath, PathDictionary.firstManufacturerSelected, driver);

            // Verifies whether the first sorting option that is selected is BMW.
            Assert.AreEqual("BMW", firstSortingOption.GetAttribute("title"));

            // Retrieves the cars container div.
            var carContainerElement = Common.FindElementBy(Common.IdentifierType.xpath, PathDictionary.carContainer, driver);

            // Verifies whether each car element contains a certain string in the title.
            Common.VerifyIfAllCarTitlesContainAString(driver, carContainerElement, "BMW");

            // Verifies whether each car element contains an image.
            Common.VerifyIfAllListedCarsHaveImages(driver, carContainerElement);

            // Verifies whether each information element belonging to a car contains text.
            Common.VerifyIfCarDataIsComplete(driver, carContainerElement);

            // Closes the browser instance.
            Common.CloseBrowser(driver);
        }
    }
}
