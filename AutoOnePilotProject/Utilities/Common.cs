﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Collections.Generic;

namespace AutoOnePilotProj.Utilities
{
    /// <summary>
    /// Contains common members for all tests.
    /// </summary>
    public class Common
    {
        /// <summary>
        /// Represents the possible identifier types.
        /// For this exercise these will do.
        /// </summary>
        public enum IdentifierType { xpath, cssSelector }

        /// <summary>
        /// Opens an empty instance of Chrome browser and maximizes it.
        /// </summary>
        /// <param name="driver">The current instance of the browser.</param>
        [SetUp]
        public static IWebDriver OpenBrowser(IWebDriver driver)
        {
            driver = new FirefoxDriver(@"D:\Firefox Driver");
            driver.Manage().Window.Maximize();

            return driver;
        }

        /// <summary>
        /// Closes the current instance of the browser.
        /// </summary>
        /// <param name="driver">The current instance of the browser.</param>
        [TearDown]
        public static void CloseBrowser(IWebDriver driver)
        {
            driver.Close();
        }

        /// <summary>
        /// Clicks on a UI element.
        /// </summary>
        /// <param name="driver">The WebDriver instance.</param>
        /// <param name="identifierPath">The path which identifies an element.</param>
        /// <param name="providedWebElement">The WebElement on which to click.</param>
        public static void ClickOnUiElement(IWebDriver driver, IdentifierType identifierType, string identifierPath)
        {
            IWebElement providedWebElement = FindElementBy(IdentifierType.xpath, identifierPath, driver);
            providedWebElement.Click();
        }

        /// <summary>
        /// Finds a UI element by type and path.
        /// </summary>
        /// <param name="identifierType">The type of the identifier.</param>
        /// <param name="identifierPath">The path of the element.</param>
        /// <param name="driver">The current instance of the browser.</param>
        /// <returns>The found element.</returns>
        public static IWebElement FindElementBy(IdentifierType identifierType, string identifierPath, IWebDriver driver)
        {
            IWebElement webElement;
            switch (identifierType)
            {
                case IdentifierType.xpath:
                    webElement = driver.FindElement(By.XPath(identifierPath));
                    break;

                case IdentifierType.cssSelector:
                    webElement = driver.FindElement(By.CssSelector(identifierPath));
                    break;

                default:
                    throw new NoSuchElementException("The identifier type is invalid.");
            }

            return webElement;
        }

        /// <summary>
        /// Checks whether all cars listed have images;
        /// throws an exception otherwise.
        /// </summary>
        /// <param name="driver">The current browser instance.</param>
        /// <param name="carContainerElement">The cars container element.</param>
        public static void VerifyIfAllListedCarsHaveImages(IWebDriver driver, IWebElement carContainerElement)
        {
            IList<IWebElement> carsWithImages = carContainerElement.FindElements(By.XPath(PathDictionary.carImages));

            foreach (IWebElement webElement in carsWithImages)
            {
                var imageIsPresent = Common.VerifyIfImageIsPresent(driver, webElement);

                Assert.IsTrue(imageIsPresent);
            }
        }

        /// <summary>
        /// Checks whether an image is present in a web element or not.
        /// </summary>
        /// <param name="driver">The current instance of the browser.</param>
        /// <param name="webElement">The web element to check.</param>
        /// <returns>True if the image is present, false otherwise.</returns>
        private static bool VerifyIfImageIsPresent(IWebDriver driver, IWebElement webElement)
        {
            var imageIsPresent = (bool)((IJavaScriptExecutor)driver)
                    .ExecuteScript(@"return arguments[0].complete && typeof arguments[0].naturalWidth != ""undefined"" && arguments[0].naturalWidth > 0", webElement);

            if (!imageIsPresent)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks whether all cars listed on the page have complete information;
        /// throws an exception otherwise.
        /// </summary>
        /// <param name="driver">The current browser instance.</param>
        /// <param name="carContainerElement">The cars container element.</param>
        public static void VerifyIfCarDataIsComplete(IWebDriver driver, IWebElement carContainerElement)
        {
            IList<IWebElement> carData = carContainerElement.FindElements(By.XPath(PathDictionary.carData));

            foreach (IWebElement webElement in carData)
            {
                Assert.IsNotEmpty(webElement.Text);
            }
        }

        /// <summary>
        /// Checks whether all cars listed on the page contain a certain string;
        /// throws an exception otherwise.
        /// </summary>
        /// <param name="driver">The current browser instance.</param>
        /// <param name="carContainerElement">The cars container element.</param>
        /// <param name="textInTitle">The string to check.</param>
        public static void VerifyIfAllCarTitlesContainAString(IWebDriver driver, IWebElement carContainerElement, string textInTitle)
        {
            IList<IWebElement> carsList = carContainerElement.FindElements(By.CssSelector(PathDictionary.carsOnPage));

            // Verifies whether every car element contains BMW in the name.
            foreach (IWebElement webElement in carsList)
            {
                Assert.IsTrue(webElement.Text.Contains(textInTitle));
            }
        }
    }
}