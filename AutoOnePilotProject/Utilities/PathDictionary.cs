﻿namespace AutoOnePilotProj.Utilities
{
    /// <summary>
    /// Contains the location of various web elements.
    /// </summary>
    public static class PathDictionary
    {
        // Represents the location of the BMW manufacturer.
        public static string bmwSpanXpath = "/html/body/div/div/div[1]/div[2]/aside/form/div/ul/li[6]/span[2]";

        // Represents the location of the first selected filter option.
        public static string firstManufacturerSelected = "/html/body/div/div/div[1]/div[2]/aside/form/div/div/span/span[1]/span/ul/li[1]";

        // Represents the location of the list of cars displayed on the page.
        public static string carContainer = @"//*[@id=""car-list""]";

        // Represents the location for each car listed on the page.
        public static string carsOnPage = @"# car-list > li:nth-child(*) > div.car-name-top";

        // Represents the location for the image of each car.
        public static string carImages = @"//div[contains(@class,'car-img')]";

        // Represents the location of each cell in the table of car data.
        public static string carData = @"//table/tbody/tr/td";
    }
}